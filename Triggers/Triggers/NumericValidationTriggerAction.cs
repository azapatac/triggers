﻿namespace Triggers.Triggers
{
    using Xamarin.Forms;

    public class NumericValidationTriggerAction : TriggerAction<Entry>
    {
        protected override void Invoke(Entry entry)
        {
            double result;
            bool isValid = double.TryParse(entry.Text, out result);
            entry.TextColor = isValid ? (Color)App.Current.Resources["BlueColor"] : (Color)App.Current.Resources["OrangeColor"];
        }
    }
}