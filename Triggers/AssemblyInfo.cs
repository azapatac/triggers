using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]

[assembly: ExportFont("FiraCodeBold.ttf", Alias = "FiraCodeBold")]
[assembly: ExportFont("FiraCodeLight.ttf")]